
Pod::Spec.new do |s|
  s.name             = 'LJTest1'
  s.version          = '1.0.8'
  s.summary          = 'A short description of LJTest1.'
  s.description      = <<-DESC
这只是一个测试用的组件，并且尝试添加图片等资源
                       DESC

  s.homepage         = 'https://gitlab.com/common52/commonspecs.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '鲁凌' => '337101067@qq.com' }
  s.source           = { :git => 'https://gitlab.com/common52/LJtest1.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'

  s.source_files = 'LJTest1/Classes/**/**/*'
  
   s.resource_bundles = {
     'LJTest1' => ['LJTest1/Assets/*.{xcassets}']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
   s.dependency 'Masonry'
end
