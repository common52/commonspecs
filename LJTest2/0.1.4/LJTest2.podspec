Pod::Spec.new do |s|
  s.name             = 'LJTest2'
  s.version          = '0.1.4'
  s.summary          = 'A short description of LJTest2.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/common52/commonspecs.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '鲁凌' => '337101067@qq.com' }
  s.source           = { :git => 'https://gitlab.com/common52/LJTest2.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'

  s.source_files = 'LJTest2/Classes/**/*'
  
   s.resource_bundles = {
     'LJTest2' => ['LJTest2/Assets/*.{xcassets}']
   }
   
   s.subspec 'LJTextView' do |a|
       a.source_files = [
       'LJTest2/Classes/LJTextView/*'
       ]
       a.resource_bundles = {
           'LJTest2' => ['LJTest2/Assets/*.{xcassets}']
       }
   end

   s.subspec 'LJTextField' do |b|
       b.source_files = [
       'LJTest2/Classes/LJTextField/*'
       ]
       b.resource_bundles = {
           'LJTest2' => ['LJTest2/Assets/*.{xcassets}']
       }
   end
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
#   s.dependency 'Masonry'
end
